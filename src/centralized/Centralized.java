package centralized;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import logist.plan.Plan;
import logist.simulation.Vehicle;
import logist.task.Task;
import logist.task.TaskSet;
import logist.topology.Topology;
import logist.topology.Topology.City;


public class Centralized {
	final private int nBest = 2;
	final private int nChildren = 10;
	final private int nInit = 70;
	
	private Random random;
	private List<SimpleVehicle> vehicles;
	public List<Task> tasks;
	private Schedule bestSchedule;
	private Topology topology;
	
	public Centralized clone() {
		Centralized centralized = new Centralized(topology, new ArrayList<Vehicle>());
		
		centralized.vehicles = new ArrayList<SimpleVehicle>(vehicles);
		centralized.tasks = new ArrayList<Task>(tasks);
		centralized.bestSchedule = bestSchedule.clone();
		return centralized;
	}
	
	public Centralized(Topology topology, List<Vehicle> vehicles) {
		this.topology = topology;
		this.vehicles = new ArrayList<SimpleVehicle>();
		this.tasks = new ArrayList<Task>();
		this.random = new Random();
		
		for (Vehicle vehicle : vehicles) {
			this.vehicles.add(new SimpleVehicle(vehicle));
		}
		
		this.bestSchedule = new Schedule(this.vehicles, this.tasks);
	}
	
	public void addTask(Task task) {
		tasks.add(task);
		bestSchedule.addTask(task);
	}
	
	public void addVehicle() {
		City homeCity = topology.cities().get(random.nextInt(topology.cities().size()));
		vehicles.add(new SimpleVehicle(getVehicleCapacity(), homeCity, getVehicleCostPerKm()));
	}
	
	public void increaseCarry(double increment) {
		double newCapacity = getVehicleCapacity() + increment;
		for (SimpleVehicle vehicle : vehicles) {
			vehicle.setCapacity(newCapacity);
		}
	}
	
	public int getVehicleCount() {
		return vehicles.size();
	}
	
	public double getVehicleCapacity() {
		return vehicles.get(0).capacity();
	}
	
	public double getVehicleCostPerKm() {
		return vehicles.get(0).costPerKm();
	}
	
	public double bestCost() {
		return bestSchedule.cost();
	}
	
	public Schedule schedule(double timeoutPlan) {
		double timeStart = System.currentTimeMillis();
    	
    	// Create initial generation
    	List<Schedule> generation = new ArrayList<Schedule>();
    	generation.addAll(bestSchedule.makeChildren(random, nInit));
    	
    	// Evolve
    	while (System.currentTimeMillis() - timeStart < timeoutPlan) {
    		List<Schedule> motherIndividuals = getNBest(generation, nBest);
    		generation.clear();
    		
    		for (Schedule mother : motherIndividuals) {
    			generation.addAll(mother.makeChildren(random, nChildren));
    		}
    		
    		Schedule minGenerationSchedule = Collections.min(generation);
    		if (minGenerationSchedule.cost() < bestSchedule.cost()) {
    			bestSchedule = minGenerationSchedule;
    		}
    	}
    	
    	return bestSchedule;
	}
	
    public List<Plan> plan(double timeoutPlan, TaskSet taskSet) {
    	Schedule schedule = schedule(timeoutPlan);
    	List<Plan> plans = new ArrayList<Plan>();
    	
    	for (SimpleVehicle vehicle : vehicles) {
    		plans.add(schedule.plan(vehicle, taskSet));
    	}
    	return plans;
    }
    
    private List<Schedule> getNBest(List<Schedule> schedules, int n) {
    	Collections.sort(schedules);
    	return new ArrayList<Schedule>(schedules.subList(0, n));
    }
}
