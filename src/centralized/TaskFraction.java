package centralized;

import logist.task.Task;

public class TaskFraction {
	enum Type {
		DELIVER, PICKUP
	}
	
	public Task task;
	public Type type;
	public TaskFraction pair;
	
	public TaskFraction(Task task, Type type) {
		this.task = task;
		this.type = type;
	}
}
