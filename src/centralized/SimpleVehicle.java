package centralized;

import logist.simulation.Vehicle;
import logist.topology.Topology.City;

public class SimpleVehicle {
	private double capacity;
	private City currentCity;
	private double costPerKm;
	
	public SimpleVehicle(Vehicle vehicle) {
		this.capacity = vehicle.capacity();
		this.currentCity = vehicle.homeCity();
		this.costPerKm = vehicle.costPerKm();
	}
	
	public SimpleVehicle(double capacity, City currentCity, double costPerKm) {
		this.capacity = capacity;
		this.currentCity = currentCity;
		this.costPerKm = costPerKm;
		System.out.println(currentCity);

	}
	
	public void setCapacity(double capacity) {
		this.capacity = capacity;
	}
	
	public double capacity() {
		return this.capacity;
	}
	
	public City getCurrentCity() {
		return currentCity;
	}
	
	public double costPerKm() {
		return costPerKm;
	}
	
	public SimpleVehicle clone() {
		return new SimpleVehicle(capacity, currentCity, costPerKm);
	}
}
