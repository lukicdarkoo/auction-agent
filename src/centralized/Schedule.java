package centralized;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;


import logist.plan.Plan;
import logist.task.Task;
import logist.task.TaskSet;
import logist.topology.Topology.City;


public class Schedule implements Comparable<Schedule> {
	private Map<SimpleVehicle, List<TaskFraction>> fullStrategy;

	private Schedule() {
		this.fullStrategy = new HashMap<SimpleVehicle, List<TaskFraction>>();
	}

	public Schedule(List<SimpleVehicle> vehicles, List<Task> tasks) {
		this.fullStrategy = new HashMap<SimpleVehicle, List<TaskFraction>>();

		// Find the vehicle with the biggest capacity
		for (SimpleVehicle vehicle: vehicles) {
			this.fullStrategy.put(vehicle, new ArrayList<TaskFraction>());
		}

		// Put all tasks to the biggest vehicle
		for (Task task: tasks) {
			addTask(task);
		}
	}

	public void addTask(Task task) {
		SimpleVehicle biggestVehicle = biggestVehicle();
		TaskFraction pickup = new TaskFraction(task, TaskFraction.Type.PICKUP);
		TaskFraction deliver = new TaskFraction(task, TaskFraction.Type.DELIVER);
		pickup.pair = deliver;
		deliver.pair = pickup;
		fullStrategy.get(biggestVehicle).add(pickup);
		fullStrategy.get(biggestVehicle).add(deliver);
	}
	
	private SimpleVehicle biggestVehicle() {
		SimpleVehicle biggestVehicle = null;
		for (SimpleVehicle vehicle: vehicles()) {
			if (biggestVehicle == null || biggestVehicle.capacity() < vehicle.capacity()) {
				biggestVehicle = vehicle;
			}
		} 
		return biggestVehicle;
	}
	
	public List<TaskFraction> getChromosome() {
		List<TaskFraction> tasks = new ArrayList<TaskFraction>();
		for (SimpleVehicle vehicle : this.fullStrategy.keySet()) {
			tasks.addAll(this.fullStrategy.get(vehicle));
			tasks.add(null);
		}
		return tasks;
	}
	
	public void setChromosome(List<TaskFraction> taskGenome) {
		int i = 0;
		for (SimpleVehicle vehicle : this.fullStrategy.keySet()) {
			this.fullStrategy.get(vehicle).clear();

			TaskFraction task = taskGenome.get(i);
			while (task != null) {
				this.fullStrategy.get(vehicle).add(task);
				task = taskGenome.get(++i);
			}
			i++;
		}
	}
	
	private SimpleVehicle findVehicle(TaskFraction task) {
		for (SimpleVehicle vehicle : vehicles()) {
			if (fullStrategy.get(vehicle).contains(task)) {
				return vehicle;
			}
		}
		return null;
	}
	
	public List<Schedule> makeChildren(Random random, int numberOfchildren) {
		List<Schedule> children = new ArrayList<Schedule>();
		List<TaskFraction> chromosome = getChromosome();
		
		for (int i = 0; i < numberOfchildren; i++) {
			List<TaskFraction> child = new ArrayList<TaskFraction>(chromosome);
			int rand1 = random.nextInt(chromosome.size() - 1);
			int rand2 = random.nextInt(chromosome.size() - 1);
			TaskFraction pairTask = null;

			if (child.get(rand1) == null && child.get(rand2) != null) {
				pairTask = child.get(rand2).pair;
			} else if (child.get(rand1) != null && child.get(rand2) == null) {
				pairTask = child.get(rand1).pair;
			}
			
			Collections.swap(child, rand1, rand2);
			
			Schedule newSched = this.clone();
			newSched.setChromosome(child);
			
			if (pairTask != null) {
				newSched.fullStrategy.get(newSched.findVehicle(pairTask)).remove(pairTask);
				newSched.fullStrategy.get(newSched.findVehicle(pairTask.pair)).add(pairTask);
			}
			
			if (newSched.checkConstraints())
				children.add(newSched);
			else
				i--;
		}
		
		return children;
	}

	private static double totalTasksWeight(List<TaskFraction> tasks) {
		double total = 0;
		for (TaskFraction task : tasks) {
			total += task.task.weight;
		}
		return total;
	}

	private static Task findOriginalTask(Task task, TaskSet taskSet) {
		if (taskSet == null) {
			return task;
		}
		
		for (Task taskOriginal : taskSet) {
			if (taskOriginal.id == task.id) {
				return taskOriginal;
			}
		}
		
		return null;
	}
	
	public Plan plan(SimpleVehicle vehicle, TaskSet taskSet) {
		List<TaskFraction> tasks = fullStrategy.get(vehicle);
		City currentCity = vehicle.getCurrentCity();
		Plan plan = new Plan(currentCity);
		for (TaskFraction task : tasks) {
			if (task.type == TaskFraction.Type.PICKUP) {
				for (City city : currentCity.pathTo(task.task.pickupCity)) {
					plan.appendMove(city);
				}
				plan.appendPickup(Schedule.findOriginalTask(task.task, taskSet));
				currentCity = task.task.pickupCity;
			} else {
				for (City city : currentCity.pathTo(task.task.deliveryCity)) {
					plan.appendMove(city);
				}
				plan.appendDelivery(Schedule.findOriginalTask(task.task, taskSet));
				currentCity = task.task.deliveryCity;
			}
		}

		return plan;
	}

	public Schedule clone() {
		Schedule newS = new Schedule();
		
		for (SimpleVehicle vehicle : fullStrategy.keySet()) {
			List<TaskFraction> tasks = fullStrategy.get(vehicle);
			newS.fullStrategy.put(vehicle, new ArrayList<TaskFraction>(tasks));
		}
		
		return newS;
	}

	public double cost() {
		double cost = 0;
		for (SimpleVehicle vehicle : vehicles()) {
			cost += plan(vehicle, null).totalDistance() * vehicle.costPerKm();
		}
		return cost;
	}

	public List<TaskFraction> tasks(SimpleVehicle vehicle) {
		return fullStrategy.get(vehicle);
	}

	public List<SimpleVehicle> vehicles() {
		List<SimpleVehicle> vehicles = new ArrayList<SimpleVehicle>();
		
		for (SimpleVehicle vehicle : fullStrategy.keySet()) {
			vehicles.add(vehicle);
		}
		
		return vehicles;
	}
	
	public boolean checkConstraints() {
		for (Map.Entry<SimpleVehicle, List<TaskFraction>> entry : fullStrategy.entrySet()) {
			SimpleVehicle vehicle = entry.getKey();
			List<TaskFraction> tasks = entry.getValue();
			List<TaskFraction> tasksInVehicle = new ArrayList<TaskFraction>();
			
			for (TaskFraction task : tasks) {
				if (task.type == TaskFraction.Type.PICKUP) {
					tasksInVehicle.add(task);
				} else {
					if (! tasksInVehicle.remove(task.pair)) {
						return false;
					}
				}
				
				if (Schedule.totalTasksWeight(tasksInVehicle) > vehicle.capacity()) {
					return false;
				}
			}
		}
		return true;
	}
	
	@Override
	public int compareTo(Schedule arg0) {
		return (int)(cost() - arg0.cost());
	} 
}
