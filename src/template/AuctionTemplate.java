package template;

import java.io.File;
//the list of imports
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import centralized.Centralized;
import centralized.SimpleVehicle;
import logist.LogistSettings;
import logist.Measures;
import logist.behavior.AuctionBehavior;
import logist.config.Parsers;
import logist.agent.Agent;
import logist.simulation.Vehicle;
import logist.plan.Plan;
import logist.task.Task;
import logist.task.TaskDistribution;
import logist.task.TaskSet;
import logist.topology.Topology;
import logist.topology.Topology.City;
import uchicago.src.sim.engine.Schedule;

/**
 * TODO:
 * - Convert costs to required units with logist.Measures.
 * - Delete all agents in agents.xml except `auction-main-34` and AuctionDummy.java (according to deliverables rules)
 */
@SuppressWarnings("unused")
public class AuctionTemplate implements AuctionBehavior {
	
	final private double MIN_RATIO = 0.8;
	final private double MAX_RATIO = 1.5;
	final private double RATIO_MUL = 1.1;
	final private int MAX_VEHICLE_NUM = 5;
	final private double ADD_CAPACITY_VEHICLE_THRESHOLD = 0.8;
	final private double CAPACITY_STEP = 5;
	final private double MIN_SIMPLE_RATIO = 0.5;
	final private double DISTANCE_MODIFIER_MIN = 0.9;
	final private double DISTANCE_MODIFIER_MAX = 1.1;
	
	private Topology topology;
	private TaskDistribution distribution;
	private Agent agent;
	private Random random;
	private City currentCity;
	
	private long timeoutBid;
	private long timeoutPlan;
	private double ratio;
	private int numberAgents;
	private int auctionId;
	
	private List<Double> ratios;
	private List<Centralized> opponentSchedulers;
	private List<Centralized> newOpponentSchedulers;
	private Centralized ourScheduler;
	private Centralized ourNewScheduler;
	
	private double[] cityFromCoeff, cityFromNewCoeff;
	private double[] cityToCoeff, cityToNewCoeff;
	private double cityFromToModifier, newCityFromToModifier;
	private double[] deliverProbabilities;
	
	private int wins = 0;

	@Override
	public void setup(Topology topology, TaskDistribution distribution, Agent agent) {
		this.topology = topology;
		this.distribution = distribution;
		this.agent = agent;
		this.auctionId = 0;
		this.ratio = MIN_RATIO;
		this.timeoutBid = 300;
		this.currentCity = agent.vehicles().get(0).homeCity();
		this.cityFromCoeff = new double[topology.size()];
		this.cityToCoeff = new double[topology.size()];
		this.cityFromToModifier = 1;
		
		this.ourScheduler = new Centralized(topology, agent.vehicles());
		this.opponentSchedulers = new ArrayList<Centralized>();
		this.newOpponentSchedulers = new ArrayList<Centralized>();
		this.random = new Random(-9019554669489983951L * currentCity.hashCode() * agent.id());
		this.numberAgents = 1;
		
		// Load timeouts from configuration file
		LogistSettings ls = null;
        try {
            ls = Parsers.parseSettings("config" + File.separator + "settings_auction.xml");
        }
        catch (Exception exc) {
            System.out.println("There was a problem loading the configuration file.");
        }
        this.timeoutPlan = ls.get(LogistSettings.TimeoutKey.PLAN) - 100;
        this.timeoutBid = ls.get(LogistSettings.TimeoutKey.BID) - 100;
		
		// Determine probabilities
		this.deliverProbabilities = new double[this.topology.size()];
		for (City to: this.topology.cities()) {
			for (City from: this.topology.cities()) {
				this.deliverProbabilities[to.id] += this.distribution.probability(from, to);
			}
		}
	}
	
	private void init() {
		for (int i = 0; i < this.numberAgents; i++) {
			List<Vehicle> vehicles = new ArrayList<Vehicle>();
			for (int j = 0; j < 2; j++) {
				Vehicle vehicle = agent.vehicles().get(random.nextInt(agent.vehicles().size()));
				vehicles.add(vehicle);
			}
			this.opponentSchedulers.add(new Centralized(topology, vehicles));
			this.newOpponentSchedulers.add(new Centralized(topology, vehicles));
		}
		
		this.ratios = new ArrayList<Double>(Collections.nCopies(this.numberAgents, this.MIN_RATIO));
	}

	@Override
	public Long askPrice(Task task) {
		this.newCityFromToModifier = modifier(task);
		
		this.ourNewScheduler = this.ourScheduler.clone();
		this.ourNewScheduler.addTask(task);

		double ourNewCost = this.ourNewScheduler.schedule(this.timeoutBid / this.numberAgents).cost();
		double ourCostDiff = ourNewCost - ourScheduler.bestCost();
				
		double bid = this.ratio * this.newCityFromToModifier * ourCostDiff;
		
		// For subsequent auctions, bid with opponent's predicted bid times their ratio if it is higher
		if (this.auctionId > 0) {
			for (int i = 0; i < this.numberAgents; i++) {
				if (i == agent.id()) {
					continue;
				}
				
				Centralized newScheduler = this.opponentSchedulers.get(i).clone();
				newScheduler.addTask(task);
				newOpponentSchedulers.set(i, newScheduler);
				double newCost = newScheduler.schedule(this.timeoutBid / this.numberAgents).cost();
				double costDiff = newCost - this.opponentSchedulers.get(i).bestCost();
				double newBid = this.ratios.get(i) * costDiff;
				
				if (bid < newBid) {
					bid = newBid;
				}
			}
		}
		
		if (bid < getSimpleCost(task) * MIN_SIMPLE_RATIO) {
			bid = getSimpleCost(task) * MIN_SIMPLE_RATIO;
		}
		System.out.println("[askPrice] " + ourCostDiff + " " + bid);
		
		return (long) Math.round(bid);
	}
	
	@Override
	public void auctionResult(Task previous, int winner, Long[] bids) {
		this.auctionId++;
		this.numberAgents = bids.length;
		
		if (auctionId == 1) {
			init();
		}

		// Modify my ratio depending on whether i won or lost
		if (winner == agent.id()) {
			wins += 1;
			this.ourScheduler = this.ourNewScheduler;
			this.ratio *= RATIO_MUL;

			this.cityFromCoeff = this.cityFromNewCoeff;
			this.cityToCoeff = this.cityToNewCoeff;
			this.cityFromToModifier = this.newCityFromToModifier;
		}
		else {
			this.opponentSchedulers.set(winner, this.newOpponentSchedulers.get(winner));
			this.ratio /= RATIO_MUL;
			System.out.println(ratio);
		}
		
		System.out.println("Wins: " + wins);
		System.out.println("[auctionResult" + agent.id() + "] Winner: " + winner + "; previous: " + previous + "; bids:");
		for (Long b: bids) {
			System.out.println("\t" + b);
		}
		
		// Modify adversary ratios if they are significantly different from our prediction
		for (int i = 0; i < this.numberAgents; i++) {
			if (i == agent.id()) {
				continue;
			}
			
			if (bids[i] > this.opponentSchedulers.get(i).bestCost()) {
				this.ratios.set(i, this.ratios.get(i) * RATIO_MUL);
			}
			else {
				this.ratios.set(i, this.ratios.get(i) / RATIO_MUL);
				
				if (bids[i] / this.opponentSchedulers.get(i).bestCost() < ADD_CAPACITY_VEHICLE_THRESHOLD) {
					Centralized curr = this.opponentSchedulers.get(i);
					if (curr.getVehicleCount() < MAX_VEHICLE_NUM) {
						curr.addVehicle();
					}
					else { 
						curr.increaseCarry(CAPACITY_STEP);
					}
				}
			}
		}
	}
	
	private double getSimpleCost(Task task) {
		Vehicle closestVehicle = null;
		for (Vehicle vehicle : agent.vehicles()) {
			if (closestVehicle == null || vehicle.homeCity().distanceTo(task.pickupCity) < closestVehicle.homeCity().distanceTo(task.pickupCity)) {
				closestVehicle = vehicle;
			}
		}
		return (task.pickupCity.distanceTo(closestVehicle.homeCity()) + 
				task.pickupCity.distanceTo(task.deliveryCity)) * 
				closestVehicle.costPerKm();
	}
	
	/*
	 * We define a modifier that will encourage the pursuit of a bid depending on how close to other tasks the given task is.
	 * For both pickup and delivery, we give each city a score depending on the distance between the city and the task.
	 * We then sum the squares of the differences between pickup and delivery scores divide by the value we obtained without the new task.
	 * This will give a factor below zero for tasks that intermingle well with other tasks, while tasks that take us away from the others
	 * or do not maintain pickups and delivers that are close to each other will be disadvantaged with a factor above zero.
	 */
	public double modifier(Task task) {
		double sum = 0;
		this.cityFromNewCoeff = this.cityFromCoeff.clone();
		this.cityToNewCoeff = this.cityToCoeff.clone();
		
		for (int i = 0; i < this.topology.size(); i++) {
			this.cityFromNewCoeff[i] += Math.exp(-task.pickupCity.distanceTo(this.topology.cities().get(i)));
			this.cityToNewCoeff[i] += Math.exp(-task.deliveryCity.distanceTo(this.topology.cities().get(i)));
			sum += Math.abs(this.cityFromNewCoeff[i] - this.cityToNewCoeff[i]);
		}
		sum = sum * (2 - deliverProbabilities[task.pickupCity.id]) / this.cityFromToModifier / this.topology.cities().size();
		return AuctionTemplate.map(sum, 0, 2, DISTANCE_MODIFIER_MIN, DISTANCE_MODIFIER_MAX);
	}
	
	private static double map(double x, double inMin, double inMax, double outMin, double outMax) {
	  return (x - inMin) * (outMax - outMin) / (inMax - inMin) + outMin;
	}

	@Override
	public List<Plan> plan(List<Vehicle> vehicles, TaskSet taskSet) {
		List<Plan> plans = this.ourScheduler.plan(timeoutPlan, taskSet);
		
		double reward = 0;
		for (Task task : taskSet) {
			reward += task.reward;
		}
		System.out.println("Reward: " + reward + "; Cost: " + this.ourScheduler.bestCost());
		
		return plans;
	}
}
